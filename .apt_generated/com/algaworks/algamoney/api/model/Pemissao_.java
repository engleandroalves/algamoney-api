package com.algaworks.algamoney.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pemissao.class)
public abstract class Pemissao_ {

	public static volatile SingularAttribute<Pemissao, Long> codigo;
	public static volatile SingularAttribute<Pemissao, String> descricao;

	public static final String CODIGO = "codigo";
	public static final String DESCRICAO = "descricao";

}

